package context

import (
	"github.com/bwmarrin/discordgo"
	"gitlab.com/anoopmsivadas/chaathan/config"
)

var embedTestMessage = discordgo.MessageEmbed{
	URL:   "https://www.gitlab.com/anoopmsivadas",
	Type:  discordgo.EmbedType(discordgo.EmbedTypeRich),
	Title: "Test Embed",
}

var embedMemeMessage = discordgo.MessageEmbed{
	URL:   meme.URL,
	Type:  discordgo.EmbedType(discordgo.EmbedTypeRich),
	Title: "",
	Image: &meme,
}

var meme = discordgo.MessageEmbedImage{
	URL:    "",
	Height: 400,
	Width:  250,
}

var HelpMessage = discordgo.MessageEmbed{
	URL:         "",
	Type:        discordgo.EmbedType(discordgo.EmbedTypeRich),
	Title:       "Chaathan's Commands",
	Description: "⚠ Product of Curiosity - WIP - Not Reliable ⚠",
}

func MakeHelpMessage() {
	var fields = []*discordgo.MessageEmbedField{}
	for _, command := range Commands {
		field := discordgo.MessageEmbedField{
			Name: command.Name,
			//  Value: "`" + config.CmdPrefix + command.CMD + " for memes from " + command.Subreddit,
			Value:  "`" + config.CmdPrefix + command.CMD + "` for " + command.Subreddit,
			Inline: true,
		}
		fields = append(fields, &field)
	}
	HelpMessage.Fields = fields
}
