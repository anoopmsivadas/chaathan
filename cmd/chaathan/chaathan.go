package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/anoopmsivadas/chaathan/config"
	"gitlab.com/anoopmsivadas/chaathan/pkg/context"
	"gitlab.com/anoopmsivadas/chaathan/pkg/handler"
)

func main() {
	err := config.LoadConfig()
	bot, err := discordgo.New("Bot " + config.Token)
	if err != nil {
		fmt.Println("Error Creating Discord Session", err)
		return
	}
	Init()
	bot.AddHandler(handler.CmdHandler)
	bot.Identify.Intents = discordgo.MakeIntent(discordgo.IntentsGuildMessages)
	err = bot.Open()
	if err != nil {
		fmt.Println("Error Opening Connection", err)
	}
	bot.UpdateStatus(0, "$help")
	fmt.Println("Bot is running. Press CTRL + C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc
	// close Discord session.
	bot.Close()
}

func Init() {
	context.NewCommand("Wholesome Memes", "meme", "wholesomememes")
	context.NewCommand("Tech Memes", "geek", "ProgrammerHumor")
	context.NewCommand("Linux Memes", "linux", "linuxmemes")
	context.NewCommand("Dank Memes", "dank", "dankmemes")
}
