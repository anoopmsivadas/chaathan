package context

type Command struct {
	Name string
	CMD  string
	// Help      string
	Subreddit string
}

var Commands []Command

//func NewCommand(name string, command string, help string, subreddit string) {
func NewCommand(name string, command string, subreddit string) {
	newCommand := Command{
		Name: name,
		CMD:  command,
		// Help:      help,
		Subreddit: subreddit,
	}
	Commands = append(Commands, newCommand)
}
