package meme

import (
	"log"
	"strings"

	//"os"
	"os/exec"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/anoopmsivadas/chaathan/pkg/context"
)

var embedTestMessage = discordgo.MessageEmbed{
	URL:   meme.URL,
	Type:  discordgo.EmbedType(discordgo.EmbedTypeRich),
	Title: "",
	Image: &meme,
}

var meme = discordgo.MessageEmbedImage{
	URL:    "",
	Height: 400,
	Width:  250,
}

func MemeReddit(command *context.Command) *discordgo.MessageEmbed {
	//cmd := exec.Command("ls")
	attempt := 0
start:
	attempt += 1
	cmd := exec.Command("python3", "./utils/scraper/reddit.py", command.Subreddit)
	out, err := cmd.Output()
	if err != nil {
		log.Fatal(err)
	}
	output := strings.Split(string(out), " ")
	path := output[0]
	author := output[1]
	post := output[2]
	if strings.HasSuffix(path, ".jpg") || strings.HasSuffix(path, ".png") || strings.HasSuffix(path, ".gif") {
		meme.URL = path
	} else if attempt < 5 {
		goto start
	}
	embedTestMessage.Title = "By " + author + " from r/" + command.Subreddit
	embedTestMessage.URL = "https://www.reddit.com" + post
	return &embedTestMessage
}
