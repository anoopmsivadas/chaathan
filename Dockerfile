FROM golang:latest

RUN go version
ADD . /go/src/app
WORKDIR /go/src/app
RUN apt-get update && apt-get install -y python3-pip
RUN make init
RUN make build