#!/usr/bin/python3
import sys
import praw

reddit = praw.Reddit(client_id='REDDIT_API_CLIENT_ID',
                     client_secret='REDDIT_API_CLIENT_SECRET', 
                     password="REDDIT_ACCOUNT_PASSWORD",
                     user_agent='YOUR_APP_NAME OR SOMETHING',
                     username='REDDIT_USERNAME')

def main():
    # source_list = ['MemesfromBNilavara', 'funny']
    subreddit = reddit.subreddit(sys.argv[1])
    meme = subreddit.random()
    print(meme.url,meme.author, meme.permalink)



if __name__ == "__main__":
    main()
