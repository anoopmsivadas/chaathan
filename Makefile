init:
	go mod vendor
	pip3 install -r requirements.txt
build:
	go mod vendor
	go build -mod=vendor -v -o bin/chaathan/chaathan ./cmd/chaathan/chaathan.go
run:
	go run -mod=vendor cmd/chaathan/chaathan.go
	# ./bin/chaathan/chaathan